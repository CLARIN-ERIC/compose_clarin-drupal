#!/usr/bin/env bash

set -e
SCRIPTS_PATH=$(realpath "$(dirname "${BASH_SOURCE[0]}")")

# shellcheck disable=SC1091
if [ -z "$SCRIPTS_PATH" ]; then
    source "./utils-lib.sh"
else
    source "${SCRIPTS_PATH}/utils-lib.sh"
fi

main () {

    VERBOSE=0
    HELP=0
    ERROR=0

    BACKUPS_DIR_NAME="backups"
    LOCAL_BACKUPS_DIR="$(realpath "${SCRIPTS_PATH}"/../${BACKUPS_DIR_NAME})"

    while [ $# -gt 0 ]
        do
        key="$1"
        case $key in
            "")
                ;;
            -h|--help)
                HELP=1
                ;;
            -v|--verbose)
                VERBOSE=1
                ;;
            *)
                ARGS_ARRAY+=("$key")
                ;;
        esac
        shift
    done

    if [ "${VERBOSE}" -eq 1 ]; then
        set -x
    fi

    if [ -n "${ARGS_ARRAY[0]}" ]; then
        case ${ARGS_ARRAY[0]} in
            init-sync)
                if [ ${#ARGS_ARRAY[@]} -eq 2 ]; then
                    case ${ARGS_ARRAY[1]} in
                        primary)
                            init_primary_sync
                            ;;
                        secondary)
                            init_secondary_sync
                            ;;
                        *)
                            echo "Invalid option passed to \"init-sync\": ${ARGS_ARRAY[1]}"
                            ERROR=1
                            HELP=1
                            ;;
                    esac
                else
                    echo "Incorrect number of arguments:"
                    echo ""
                    ERROR=1
                    HELP=1
                fi
                ;;
            *)
                echo "Subcommand not available in ${0#./} script"
                exit 1
                ;;
        esac
    fi

    if [ "${HELP}" -eq 1 ]; then
    	echo "  The following custom sub-command is used to initialize the synchronization setup between multiple instances running on different hosts."
        echo "  It must be run with superuser permissions"
    	echo ""
        echo "    init-sync  <\"primary\"|\"secondary\">                          Initializes the setup for SSH key-based sychronization on this host. (Must be"
        echo "                                                                run with superuser permissions)"
        echo "                                                                  - \"primary\":"
        echo "                                                                      1. Creates a new SSH key to be used by the synchronization process."
        echo "                                                                      2. Adds a new entry to \`~/.ssh/config\` using the newly generated key"
        echo "                                                                      and the specified SSH remote user and IP (from the secondary)."
        echo "                                                                      3. Creates the \`allowedIPs.txt\` file from \`allowedIPs-template.txt\`"
        echo "                                                                      and adds the specified secondary host IP to it."
        echo "                                                                      4. Adds a new entry to the system crontab \`/etc/crontab\` to perform"
        echo "                                                                      the synchronization once a day."
        echo "                                                                  - \"secondary\":"
        echo "                                                                      1. Creates a new system user for the synchronization process."
        echo "                                                                      2. Adds a new entry to the new user's home: \`~/.ssh/authorized_keys\`"
        echo "                                                                      using the specified SSH public key (obtained from the primary when using"
        echo "                                                                      \`init-sync primary\`) and the local \`ssh-remote-handler.sh\` script as"
        echo "                                                                      connection handler."
        echo "                                                                      3. Creates the \`allowedIPs.txt\` file from \`allowedIPs-template.txt\`"
        echo "                                                                      and adds the specified primary host IP to it."
        echo ""
        if [ "${ERROR}" -eq 0 ]; then
            exit 0
        else
            exit 1
        fi
    fi
}

exit_not_root() {
    if [ "$(whoami)" != "root" ]; then
        echo "ERROR: This sub-command must be run with superuser permissions!"
        exit 1
    fi
}

init_primary_sync() {
    exit_not_root
    # Request user to input all necessary parameters
    TARGET_HOST_IP=""
    read -r -e -p $'Enter the IP address of the remote secondary host:\n(none) > ' response
    if [ -n "${response}" ] && [[ "${response}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        TARGET_HOST_IP="${response}"
    else
        echo "Invalid host IP: ${response}"
        exit 1
    fi

    TARGET_HOST_DIR=""
    read -r -e -p $'Enter the Drupal installation top-level directory path on the remote secondary host:\n(none) > ' response
    if [ -n "${response}" ]; then
        TARGET_HOST_DIR="${response}"
    else
        echo "Invalid remote directory: ${response}"
        exit 1
    fi 

    TARGET_SSH_PORT="22"
    read -r -e -p $'Enter the SSH port to use for the synchronization:\n(22) > ' response
    if [ -n "${response}" ]; then
        TARGET_SSH_PORT="${response}"
    fi
    if [[ ! "${TARGET_SSH_PORT}" =~ ^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$ ]]; then
        echo "Invalid SSH port: ${response}"
        exit 1
    fi

    CONTROL_USER="deploy"
    if [ -n "$SUDO_USER" ]; then
        CONTROL_USER="$SUDO_USER"
    fi
    CONTROL_USER_HOME=$(getent passwd "${CONTROL_USER}" | cut -d: -f6)

    get_sync_user "Enter the SSH user responsible for the synchronization on the remote host:"

    # Generate the SSH key pair
    ssh-keygen -q -t ed25519 -b 4096 -N "" -C "${SYNC_USER}" -f "${CONTROL_USER_HOME}/.ssh/${SYNC_USER}"
    chown "${CONTROL_USER}:${CONTROL_USER}" "${CONTROL_USER_HOME}/.ssh/${SYNC_USER}"
    chmod 0600 "${CONTROL_USER_HOME}/.ssh/${SYNC_USER}"
    echo ""
    echo "The primary public key is:"
    cat "${CONTROL_USER_HOME}/.ssh/${SYNC_USER}.pub"
    echo "Please enter this string on every secondary host when asked for the \"primary public key\" while running \`init-sync secondary\`."
    echo ""

    # Update .ssh/config
    SSH_CONFIG_ENTRY="
Host ${TARGET_HOST_IP}
    Port ${TARGET_SSH_PORT}
    User ${SYNC_USER}
    IdentityFile ~/.ssh/${SYNC_USER}
    AddressFamily inet
    GSSAPIAuthentication no" 
    echo "${SSH_CONFIG_ENTRY}" >> "${CONTROL_USER_HOME}/.ssh/config"
    echo "The following entry has been added to the ${CONTROL_USER} \`${CONTROL_USER_HOME}/.ssh/config\` file:"
    echo "${SSH_CONFIG_ENTRY}"
    chown "${CONTROL_USER}:${CONTROL_USER}" "${CONTROL_USER_HOME}/.ssh/config"
    chmod 0600 "${CONTROL_USER_HOME}/.ssh/config"

    # Initialize allowedIPs.txt file
    init_allowedIPs_file "${TARGET_HOST_IP}"
    chown "${CONTROL_USER}:${CONTROL_USER}" "${SCRIPTS_PATH}/../allowedIPs.txt"
    chmod 0600 "${CONTROL_USER_HOME}/.ssh/${SYNC_USER}"

    if [ -f /etc/crontab ]; then
        CRONTAB_ENTRY="0 1 * * * ${CONTROL_USER} cd $(realpath "${SCRIPTS_PATH}"/../..) && bash control.sh $(basename "$(realpath "${SCRIPTS_PATH}"/..)") push-sync ${TARGET_HOST_IP} ${TARGET_HOST_DIR} > ${LOCAL_BACKUPS_DIR}/remote-sync.cron.log 2>&1 || (echo \"cron job failed with exit status $?\"; cat ${LOCAL_BACKUPS_DIR}/remote-sync.cron.log)"
        echo "" >> /etc/crontab
        echo "${CRONTAB_ENTRY}" >> /etc/crontab
        echo "The following entry has been added to the system crontab \`/etc/crontab\`. Manually edit this file to tune the cronjob parameters."
        echo "${CRONTAB_ENTRY}"
    fi
    echo ""
    echo "Initialization complete!"

    exit 0
}

init_secondary_sync() {
    exit_not_root
    # Request user to input all necessary parameters
    SOURCE_HOST_IP=""
    read -r -e -p $'Enter the IP address of the remote primary host:\n(none) > ' response
    if [ -n "${response}" ] && [[ "${response}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        SOURCE_HOST_IP="${response}"
    else
        echo "Invalid host IP: ${response}"
        exit 1
    fi

    SSH_AUTH_KEY=""
    read -r -e -p $'Enter the primary public key used by the primary host to connect to this host:\n(none) > ' response
    if [ -n "${response}" ] && [[ "${response}" =~ ^ssh-ed25519\ [^[:space:]]+\ [a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$ ]]; then
        SSH_AUTH_KEY="${response}"
    else
        echo "Invalid SSH public key: ${response}"
        echo "Please insert a public key string as displayed on the primary server when initializing the synchronization 'sync-init primary'"
        exit 1
    fi

    get_sync_user "Enter a new local user to be responsible for the synchronization on this host:"

    CONTROL_USER="deploy"
    if [ -n "$SUDO_USER" ]; then
        CONTROL_USER="$SUDO_USER"
    fi
    CONTROL_USER_HOME=$(getent passwd "${CONTROL_USER}" | cut -d: -f6)

    # Create sync user on the $CONTROL_USER group
    useradd "${SYNC_USER}"
    usermod -aG "${CONTROL_USER}" "${SYNC_USER}"
    SYNC_USR_HOME=$(getent passwd "${SYNC_USER}" | cut -d: -f6)
    mkdir -p "${SYNC_USR_HOME}/.ssh"
    chmod 0700 "${SYNC_USR_HOME}/.ssh"
    touch "${SYNC_USR_HOME}/.ssh/authorized_keys"
    chmod 0644 "${SYNC_USR_HOME}/.ssh/authorized_keys"
    chown -R "${SYNC_USER}:${SYNC_USER}" "${SYNC_USR_HOME}"

    # Update .ssh/authorized_keys
    AUTH_KEYS_ENTRY="command=\"bash ${SCRIPTS_PATH}/ssh-remote-handler.sh\",no-port-forwarding,no-x11-forwarding,no-agent-forwarding ${SSH_AUTH_KEY}"
    echo "" >> "${SYNC_USR_HOME}"/.ssh/authorized_keys
    echo "${AUTH_KEYS_ENTRY}" >> "${SYNC_USR_HOME}"/.ssh/authorized_keys
    echo ""
    echo "The following entry has been added to the ${SYNC_USER} \`${SYNC_USR_HOME}/.ssh/authorized_keys\` file:"
    echo "${AUTH_KEYS_ENTRY}"
    chown "${SYNC_USER}:${SYNC_USER}" "${SYNC_USR_HOME}/.ssh/authorized_keys"
    chmod 0600 "${SYNC_USR_HOME}/.ssh/authorized_keys"

    # Initialize allowedIPs.txt file
    init_allowedIPs_file "${SOURCE_HOST_IP}"
    chown "${CONTROL_USER}:${CONTROL_USER}" "${SCRIPTS_PATH}/../allowedIPs.txt"
    chmod 0640 "${SCRIPTS_PATH}/../allowedIPs.txt"

    # Initialize alias function
    echo "" >> "${SYNC_USR_HOME}"/.bashrc
    ALIAS_FUNCTION="
# Allow this user to run a Drupal restore as \"${CONTROL_USER}\" via sudo.
bash() {
    if [[ \$@ == \"control.sh drupal restore latest\" ]]; then
        command sudo -u \"${CONTROL_USER}\" bash \"\$@\"
    else
        command bash \"\$@\"
    fi
}
"
    echo "${ALIAS_FUNCTION}" >> "${SYNC_USR_HOME}"/.bashrc
    echo "The following entry has been added to the ${SYNC_USER} \`${SYNC_USR_HOME}/.bashrc\` file:"
    echo "${ALIAS_FUNCTION}" 

    # Initialize sudoers entry
    echo "" >> /etc/sudoers
    SUDOERS_ENTRY="## Drupal sync auto generated entries:
Cmnd_Alias DRUPAL_SYNC = /bin/bash control.sh drupal restore latest
${SYNC_USER} ALL=(${CONTROL_USER}) NOPASSWD: DRUPAL_SYNC" 
    echo "${SUDOERS_ENTRY}" >> /etc/sudoers
    echo "The following entry has been added to the system sudoers file:"
    echo "${SUDOERS_ENTRY}"
    echo ""
    echo "Initialization complete!"

    exit 0
}

get_sync_user() {
    SYNC_USER="drupalsync"
    read -r -e -p "${1}
(drupalsync) > " response
    if [ -n "${response}" ]; then
        SYNC_USER="${response}"
    fi
    if [[ ! "${SYNC_USER}" =~ ^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$ ]]; then
        echo "Invalid username: ${SYNC_USER}"
        exit 1
    fi
}

init_allowedIPs_file() {
    if [ ! -f "${SCRIPTS_PATH}/../allowedIPs.txt" ]; then 
        cp -p "${SCRIPTS_PATH}/allowedIPs-template.txt" "${SCRIPTS_PATH}/../allowedIPs.txt"
        chmod 0700 "${SCRIPTS_PATH}/../allowedIPs.txt"
    fi
    echo "" >> "${SCRIPTS_PATH}/../allowedIPs.txt"
    echo "${1}" >> "${SCRIPTS_PATH}/../allowedIPs.txt"
    echo ""
    echo "IP address: ${1} added to $(realpath "${SCRIPTS_PATH}"/../allowedIPs.txt). Manually edit this file to add extra IP addresses."
    echo ""
}

main "$@"; exit
