#!/usr/bin/env bash
# Utility methods for use in restore.sh and custom.sh

is_timestamp_valid() {
    # Validate timestamp
    if [ -z "${1}" ]; then
        echo "Backup timestamp must be supplied"
        return 1
    fi
    if [[ ! "${1}" =~ ^([0-9]+.[0-9]+|latest)$ ]]; then
        echo "Invalid timestamp: \"${1}\""
        return 1
    fi
    return 0
}

is_ip_allowed() {
    if [[ ! "${1}" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        echo "Invalid IP: ${1}"
        return 1
    fi
    if [[ ! " ${ALLOWED_SYNC_IPS[*]} " =~ ${1} ]]; then
        echo "IP address not allowed in allowedIPs.txt: ${1}"
        return 1
    fi
    return 0
}

get_allowed_sync_hosts() {
    # Get ALLOWED_SYNC_IPS array
    ALLOWED_SYNC_IPS_FILE="$(dirname "${0}")/../allowedIPs.txt"
    if [ ! -f "${ALLOWED_SYNC_IPS_FILE}" ]; then
        echo "allowedIPs.txt not found in project parent location."
        return
    fi
    ALLOWED_SYNC_IPS_FILE="$(realpath "$(dirname "${0}")"/../allowedIPs.txt)"
    while read -r line || [ -n "$line" ]; do
        if [[ "$line" =~ ^#.*$ ]]; then
        	continue
        fi
        ALLOWED_SYNC_IPS+=("$line")
    done < "${ALLOWED_SYNC_IPS_FILE}"
}

get_allowed_sync_hosts