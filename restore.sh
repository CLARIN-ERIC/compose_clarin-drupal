#!/usr/bin/env bash
set -e

SCRIPTS_PATH=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
source "${SCRIPTS_PATH}/utils-lib.sh"

main () {

    VERBOSE=0
    HELP=0
    ERROR=0

    while [ $# -gt 0 ]
        do
        key="$1"
        case $key in
            "")
                ;;
            -h|--help)
                HELP=1
                ;;
            -v|--verbose)
                VERBOSE=1
                ;;
            *)
                ARGS_ARRAY+=("$key")
                ;;
        esac
        shift
    done

    if [ "${VERBOSE}" -eq 1 ]; then
        set -x
    fi

    if [ -n "${ARGS_ARRAY[0]}" ]; then
        case ${ARGS_ARRAY[0]} in
            ls)
                "${SCRIPTS_PATH}"/list_backups.sh
                exit 0
                ;;
            all|db|www)
                # Optional commands: ARGS_ARRAY[1] must contain the timestamp string or "latest"
                if [ -z "${ARGS_ARRAY[1]}" ]; then
                    echo "ERROR: One of: <timestamp> or \"lastest\" must be supplied"
                    "${SCRIPTS_PATH}"/list_backups.sh
                    exit 1
                fi
                if ! is_timestamp_valid "${ARGS_ARRAY[1]}"; then
                    echo "ERROR: ${ARGS_ARRAY[1]} is not a valid <timestamp>"
                    "${SCRIPTS_PATH}"/list_backups.sh
                    exit 1
                fi
                RESTORE_OPTS=("${ARGS_ARRAY[0]}")
                # The image internal restore script restores the supplied <timestamp> file, or "lastest" if called without parameters
                if [ "${ARGS_ARRAY[1]}" != "latest" ]; then
                    RESTORE_OPTS+=("${ARGS_ARRAY[1]}")
                fi
                run_restore
                exit 0
                ;;
            latest|*)
                # When only timestamp|latest is supplied, restore all by default 
                if [ -z "${ARGS_ARRAY[1]}" ] && is_timestamp_valid "${ARGS_ARRAY[0]}"; then
                    echo "Restoring both database and web files backups by default"
                    RESTORE_OPTS=("all")
                    # The image internal restore script restores the supplied <timestamp> file, or "lastest" if called without parameters
                    if [ "${ARGS_ARRAY[0]}" != "latest" ]; then
                        RESTORE_OPTS+=("${ARGS_ARRAY[0]}")
                    fi
                    run_restore
                    exit 0
                else
                    echo "Invalid parameter: ${ARGS_ARRAY[0]}"
                    HELP=1
                    ERROR=1
                fi
                ;;
        esac
    fi

    if [ "${HELP}" -eq 1 ]; then
        echo "Restore the state of Drupal from backups."
        echo ""
        echo "Usage: ${0#./} [-h|-v] [all|db|www] ls|latest|<timestamp>"
        echo ""
        echo "  Optional Commands:"
        echo "    all                     Default. Restore both: database and web files backups."
        echo "    db                      Restore only the database backup."
        echo "    www                     DeRestore only the web files backup.fault"
        echo ""
        echo "  Commands:"
        echo "    ls                      List available backups."
        echo "    latest                  Restore the latest available backups."
        echo "    <timestamp>             Restore the backups identified by <timestamp> as displayed by \`restore.sh ls\`"
        echo ""
        echo "  Optional flags:"
        echo "    -v, --verbose           Run in verbose mode"
        echo "    -h, --help              Show help"
        echo ""
        if [ "${ERROR}" -eq 0 ]; then
            exit 0
        else
            exit 1
        fi
    fi
}

run_restore() {
    COMPOSE_DIR=$(find . -name clarin -type d -exec sh -c  'echo "$1" ; if [ -f docker-compose.yml ]; then dirname "$1"; fi' shell {} \;)
    cd "$COMPOSE_DIR"
    # Stop crond for the restore to avoid cron failure emails being sent during the restore
    
    # bugfix with < /dev/null see: https://github.com/docker/compose/issues/8833
    docker-compose exec -T fpm_drupal supervisorctl -u sysops -p thepassword stop crond < /dev/null
    docker-compose exec -T fpm_drupal /usr/bin/restore-drupal.sh "${RESTORE_OPTS[@]}" "${VERBOSE_ARG}" < /dev/null
    docker-compose exec -T fpm_drupal supervisorctl -u sysops -p thepassword start crond < /dev/null
}

main "$@"; exit
