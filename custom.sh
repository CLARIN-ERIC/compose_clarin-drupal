#!/usr/bin/env bash
set -e

SCRIPTS_PATH=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
# shellcheck disable=SC1091
source "${SCRIPTS_PATH}/utils-lib.sh"

main () {

    VERBOSE=0
    HELP=0
    ERROR=0

    BACKUPS_DIR_NAME="backups"
    DB_BACKUPS_SUBDIR="database"
    HTTP_BACKUPS_SUBDIR="htdocs"
    LOCAL_BACKUPS_DIR="$(realpath "${SCRIPTS_PATH}"/../${BACKUPS_DIR_NAME})"

    while [ $# -gt 0 ]
        do
        key="$1"
        case $key in
            "")
                ;;
            -h|--help)
                HELP=1
                ;;
            -v|--verbose)
                VERBOSE=1
                ;;
            *)
                ARGS_ARRAY+=("$key")
                ;;
        esac
        shift
    done

    if [ "${VERBOSE}" -eq 1 ]; then
        set -x
        VERBOSE_ARG="-v"
    fi

    if [ -n "${ARGS_ARRAY[0]}" ]; then
        case ${ARGS_ARRAY[0]} in
            push-backup)
                if [ ${#ARGS_ARRAY[@]} -eq 4 ]; then
                    validate_targetip "${ARGS_ARRAY[1]}"
                    if ! is_timestamp_valid "${ARGS_ARRAY[3]}"; then
                        echo "Aborting ..."
                        exit 1
                    fi
                    restore_to_remote "${ARGS_ARRAY[@]:1:3}"
                    exit 0
                else
                    echo "Incorrect number of arguments:"
                    echo ""
                    ERROR=1
                    HELP=1
                fi
                ;;
            push-sync)
                if [ ${#ARGS_ARRAY[@]} -eq 3 ]; then
                    validate_targetip "${ARGS_ARRAY[1]}"
                    backup 
                    restore_to_remote "${ARGS_ARRAY[@]:1:2}" "latest"
                    exit 0
                else
                    echo "Incorrect number of arguments:"
                    echo ""
                    ERROR=1
                    HELP=1
                fi
                ;;
            *)
                echo "Subcommand not available in ${0#./} script"
                exit 1
                ;;
        esac
    fi

    if [ "${HELP}" -eq 1 ]; then
    	echo "  The following custom sub-commands can be used on synchronized multi-instance setups. Disregard them if this is a local stand-alone installation."
    	echo ""
        echo "    push-backup  <ip_addr> <remote_dir> <timestamp|\"latest\">    Push the backup specified by <timestamp> to the CLARIN Drupal deployment"
        echo "                                                                  running remotely at <ip_addr> and deployed on <remote_dir>."
        echo "                                                                  The keyword \"latest\" can be used instead of <timestamp> to specify the"
        echo "                                                                  most recent available backup."
        echo "    push-sync  <ip_addr> <remote_dir>                           Push the current state to the CLARIN Drupal deployment running remotely at"
        echo "                                                                  <ip_addr> and deployed at <remote_dir>."
        echo "                                                                  This is the same as \`push-backup\` except that a fresh local backup is"
        echo "                                                                  created and restored on the remote."
        echo ""
        if [ "${ERROR}" -eq 0 ]; then
            exit 0
        else
            exit 1
        fi
    fi
}

validate_targetip() {
    # Validate remote IP address
    if [ -z "${1}" ]; then
        echo "Remote IP address must be supplied"
        exit 1
    fi
    # Make sure remote address IP is allowed
    if ! is_ip_allowed "${1}"; then
        echo "Aborting ..."
        exit 1
    fi
}

restore_to_remote() {
    send_backup "${1}" "${2}" "${3}"
    exec_remote_restore "${1}" "${2}"
}

backup() {
    echo "Creating a local backup."
    if [ -f "${SCRIPTS_PATH}/../../control.sh" ]; then
        # use sed to remove original log information otherwise the control.sh script will duplicate it
        (cd "${SCRIPTS_PATH}/../.." && ./control.sh "$(basename "$(realpath "${SCRIPTS_PATH}/..")")" backup "${VERBOSE_ARG}" | sed -E 's/\[.+]+ //g')
    else
        (cd "$(dirname "${0}")" && ./backup.sh)
    fi
}

send_backup() {
    TIMESTAMP="${3}"
    if [ "$TIMESTAMP" == "latest" ]; then
        BACKUPDB_FILE=${LOCAL_BACKUPS_DIR}/${DB_BACKUPS_SUBDIR}/$(basename "$(unset -v latest; for file in "${LOCAL_BACKUPS_DIR}/${DB_BACKUPS_SUBDIR}"/backup_drupal_db_*.sql.gz; do [ ! -L "$file" ] && [[ $file -nt $latest ]] &&  latest=$file; done; echo "$latest")")
        BACKUPWWW_FILE=${LOCAL_BACKUPS_DIR}/${HTTP_BACKUPS_SUBDIR}/$(basename "$(unset -v ltst; for file in "${LOCAL_BACKUPS_DIR}/${HTTP_BACKUPS_SUBDIR}"/backup_drupal_www_*.tar.gz; do [ ! -L "$file" ] && [[ $file -nt $ltst ]] &&  ltst=$file; done; echo "$ltst")")
        if [[ $BACKUPWWW_FILE =~ ^[^[:space:]]+_([0-9]+.[0-9]+)\.[^[:space:]]{1,3}\.gz$ ]]; then 
            TIMESTAMP=${BASH_REMATCH[1]}
        else 
            echo "No timestamp match found"
            exit 1
        fi
    else
        BACKUPDB_FILE=$(find "${LOCAL_BACKUPS_DIR}" -name backup_drupal_db_"${TIMESTAMP}".sql.gz 2> /dev/null)
        BACKUPWWW_FILE=$(find "${LOCAL_BACKUPS_DIR}" -name backup_drupal_www_"${TIMESTAMP}".tar.gz 2> /dev/null)
    fi

    HOST_STRING=$(hostname)

    echo "Sending database backup: ${BACKUPDB_FILE##*/} ... "
    rsync --progress "${BACKUPDB_FILE}" "${1}:${2%/}/${BACKUPS_DIR_NAME}/${DB_BACKUPS_SUBDIR}/backup_drupal_db_-$HOST_STRING-_$TIMESTAMP.sql.gz" # see: https://forum.openwrt.org/t/ash-usr-libexec-sftp-server-not-found-when-using-scp/125772
    ssh "${1}" ln -fs "backup_drupal_db_-$HOST_STRING-_$TIMESTAMP.sql.gz" "${2%/}/${BACKUPS_DIR_NAME}/${DB_BACKUPS_SUBDIR}/backup_drupal_db.sql.gz"

    echo "Sending web files backup: ${BACKUPWWW_FILE##*/} ... "
    rsync --progress  "${BACKUPWWW_FILE}" "${1}:${2%/}/${BACKUPS_DIR_NAME}/${HTTP_BACKUPS_SUBDIR}/backup_drupal_www_-$HOST_STRING-_$TIMESTAMP.tar.gz"
    ssh "${1}" ln -fs "backup_drupal_www_-$HOST_STRING-_$TIMESTAMP.tar.gz" "${2%/}/${BACKUPS_DIR_NAME}/${HTTP_BACKUPS_SUBDIR}/backup_drupal_www.tar.gz"
    echo "Backups ready to restore on remote"
}

exec_remote_restore() {
    REMOTE_PROJECT_DIR=$(basename "${2%/}")
    # use sed to remove original log information otherwise the control.sh script will duplicate it
    ssh -tt "${1}" "cd $(dirname "${2%/}") && bash control.sh ${REMOTE_PROJECT_DIR} restore latest" | sed -E 's/\[.+]+ //g'
     
}

main "$@"; exit
