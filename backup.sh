#!/usr/bin/env bash
set -e

main() {
    VERBOSE=0
    HELP=0
    ERROR=0

    while [ $# -gt 0 ]
        do
        key="$1"
        case $key in
            "")
                ;;
            -h|--help)
                HELP=1
                ;;
            -v|--verbose)
                VERBOSE=1
                ;;
            *)
                echo "Unkown option: $key"
                HELP=1
                ERROR=1
                ;;
        esac
        shift
    done

    if [ "${VERBOSE}" -eq 1 ]; then
        set -x
        VERBOSE_ARG="-v"
    fi

    if [ "${HELP}" -eq 1 ]; then
        echo "Backup the state of Drupal. Both: database and Drupal web files are backed up."
        echo "backup.sh [-hd]"
        echo ""
        echo "  -v, --verbose             Run in verbose mode"
        echo ""
        echo "  -h, --help                Show help"
        echo ""
        if [ "${ERROR}" -eq 0 ]; then
            exit 0
        else
            exit 1
        fi
    fi
    run_backup
}

run_backup() {
    COMPOSE_DIR=$(find . -name clarin -type d -exec sh -c  'echo "$1" ; if [ -f docker-compose.yml ]; then dirname "$1"; fi' shell {} \;)
    cd "$COMPOSE_DIR"
    # Backups timestamp is normally supplied by control script
    if [ -z "${BACKUPS_TIMESTAMP}" ]; then
        BACKUPS_TIMESTAMP=$(date +%Y%m%dT%H%M)
    fi
    # bugfix with < /dev/null see: https://github.com/docker/compose/issues/8833
    docker-compose exec -T fpm_drupal bash -c "BACKUPS_TIMESTAMP=${BACKUPS_TIMESTAMP} /usr/bin/backup-drupal.sh ${VERBOSE_ARG}" < /dev/null
}

main "$@"; exit
