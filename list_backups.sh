#!/usr/bin/env bash
set -e

main () {

    VERBOSE=0
    HELP=0
    ERROR=0

    while [ $# -gt 0 ]
        do
        key="$1"
        case $key in
            "")
                ;;
            -h|--help)
                HELP=1
                ;;
            -v|--verbose)
                VERBOSE=1
                ;;
            *)
                echo "Invalid parameter: $key"
                HELP=1
                ERROR=1
                ;;
        esac
        shift
    done

    if [ "${VERBOSE}" -eq 1 ]; then
        set -x
    fi

    if [ "${HELP}" -eq 1 ]; then
        echo "Lists the available backups from Drupal."
        echo "Each backup is composed by two files: a database backup and a drupal files backup"
        echo ""
        echo "Usage: ${0#./}"
        echo ""
        echo ""
        if [ "${ERROR}" -eq 0 ]; then
            exit 0
        else
            exit 1
        fi
    fi
    list_backups
}

list_backups() {
    # Find the loaction of the backups relative to this script
    BACKUPS_DIRP=$(realpath "$(dirname "${BASH_SOURCE[0]}")")/..
    cd "${BACKUPS_DIRP}"

    ls_opts="-T"
    if [[ $(uname) == 'Linux' ]]; then
        ls_opts="--time-style=\"+%b %d %H:%M:%S %Y\""
    fi

    echo ""
    echo "Available backup files:"
    echo ""
    printf "%-15s %-75s %s\n" "Date:" "File name:" "Timestamp:"
    find backups -type f  -name "backup_*.gz" -exec bash -c \
    'ls -l '"$ls_opts"' $1' shell {} \; | awk -F ' ' '{
     filepath=$10;
     printf "%-7s", $6 " " $7 ;
     printf "%-9s", $9 ;
     sub(/^backups\/.+\//,"",$10); printf "%-76s", $10 ;
     match($10, /([0-9]+T?[0-9]+)/); printf "%-15s\n", substr($10, RSTART, RLENGTH);}' \
    | sort  -k3,3nr -k1,1Mr -k2,2nr -k5,5r -k4,4r | sed "1,2s/\(.*\)/\1 (latest)/"
    echo ""
    echo ">>>>>>>>>>>>>>>>>>>>>>>ATTENTION<<<<<<<<<<<<<<<<<<<<<<"
    echo "THIS PROJECT USES A CUSTOM RESTORE SYNTAX, READ BELLOW"
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    echo ""
    echo "Restore syntax:"
    echo "- control.sh restore [all|db|www] <latest>|<timestamp>"
    echo ""
    echo "Examples:"
    echo "- control.sh restore latest            Restore the lastest backups available for both: database and web files."
    echo "                                        -> same as: control.sh restore all latest"
    echo "- control.sh restore <timestamp>       Restore the backups identified by <timestamp> for both: database and web files."
    echo "- control.sh restore db <timestamp>    Restore the database backup identified by <timestamp>."
    echo "- control.sh restore www latest        Restore the latest available web files backup."
    echo ""
    echo ""
}

main "$@"; exit
