#!/usr/bin/env bash
set -e

SCRIPTS_PATH=$(realpath "$(dirname "${BASH_SOURCE[0]}")")
# shellcheck disable=SC1091
source "${SCRIPTS_PATH}/utils-lib.sh"

main() {
    validate_request
    run_command
}

validate_request() {
    if ! is_sship_allowed; then
        echo "Rejected: remote IP not allowed"
        exit 1
    fi
    if ! is_command_allowed; then
        echo "Rejected: command not allowed"
        exit 1
    fi
}

is_sship_allowed() {
    # For SSH call make sure requester's IP is allowed
    if [ -n "${SSH_CLIENT}" ]; then
        REQUESTER_IP="${SSH_CLIENT%% *}"
        if ! is_ip_allowed "${REQUESTER_IP}"; then
           echo "Aborting ..."
           return 1
        fi
    else
        echo "ERROR: SSH_CLIENT variable not set."
        return 1
    fi
    return 0
}

is_command_allowed() {
    # /backups directory might be a symlink. Use -s to avoid expansion
    PROJECT_PARENT_DIR=$(basename "$(realpath -s "${SCRIPTS_PATH}"/..)")
    BACKUPS_PATH=$(realpath -s "${SCRIPTS_PATH}"/../backups)
    DEPLOY_PATH=$(realpath "${SCRIPTS_PATH}"/../..)
    grep -E -q \
      -e "^rsync --server -e\.LsfxCIvu --log-format=X \. ${BACKUPS_PATH//\//\\/}\/(htdocs|database)\/backup_drupal_(www|db)_(-[a-zA-Z0-9._-]+-_)?([0-9]+.[0-9]+)\.(tar|sql)\.gz$" \
      -e "^ln -fs backup_drupal_(www|db)_(-[a-zA-Z0-9._-]+-_)?([0-9]+.[0-9]+)\.(tar|sql)\.gz ${BACKUPS_PATH//\//\\/}\/(htdocs|database)\/backup_drupal_(www|db)\.(tar|sql)\.gz$" \
      -e "^cd ${DEPLOY_PATH//\//\\/} && bash control\.sh ${PROJECT_PARENT_DIR} restore latest$" <<< "${SSH_ORIGINAL_COMMAND}"
    GREP_RETURN=$?
    if [ ${GREP_RETURN} -eq 1 ]; then
        echo "Command not allowed: ${SSH_ORIGINAL_COMMAND}"
        return 1
    fi
    return 0
}

run_command() {
	# Use a login shell for potential user aliases
    bash -l -c "${SSH_ORIGINAL_COMMAND}"
}

main "$@"; exit
